# elisp-shortcuts-libre

Some symbol-using shortcuts for lisp code.

The idea was for example,

    (mapcar  (λ (x) (≠ x 5))  num-list)

Instead of:

    (mapcar  (lambda (x) (not (= x 5)))  num-list)

But aliasing lambda seems tricky -- or at least I don't know how to do it properly.
The obvious attemps:

    (defalias 'λ 'lambda)
    
and:

    (defmacro λ (&rest cdr) `(lambda ,@cdr))
    
did not work for me in code involving (local-set-key...).

'libre' in the name refers to freely using non-ascii characters.


## Provides Features

* shortcut-libre        a few self-explanatory shortcuts
* shortcut-extra-libre  some more, perhaps less obvious shortcuts


## To consider

* How to actually make λ be able to substitute fully for lambda.
* A shortcut-math-libre set of shortcuts for math (hopefully can play well with calc-mode).


## Author

Paul Horton


## License

GNU Public License
