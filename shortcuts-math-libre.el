;; shortcuts-math-libre.el ---  math related unicode symbols   -*- lexical-binding: t -*-

;; Copyright (C) 2019, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20190414
;; Updated: 20190414
;; Version: 0.0
;; Keywords:

;;; Commentary:

;; Place holder file for possible math shortcuts, such as

;; possible ∛，∜, or even ∫ etc.
;;
;; Design should perhaps consider use with calc-mode...


;;; Change Log:

;;; Code:


(require 'macroexp)
(require 'let1)
(require 'shortcuts)
(require 'shortcuts-math)
(require 'shortcuts-math-symbols-basic)


(defvaralias '𝑒 'float-e "Eulers,Napier,Bernouli constant")

(defvaralias 'π 'float-pi    "圓周率.  π from geometry")
(defvar ２π (* 2 float-pi) "２×圓周率.  2 times π from geometry")
(defvar ½π (* 0.5 float-pi)  "圓周率/2.  ½ times π from geometry")


(defalias '√ 'sqrt)





(defun x² (x)
  "Function to square X"
  (* x x))

(defmacro ² (x)
  "Macro to square expression X"
  (macroexp-let2 nil sym x
   `(* ,sym ,sym)))


(defmacro x³ (x)
  "Cube x"
  (let ((x-val (cl-gensym)))
    `(progn
       (setq ,x-val ,x);  evaluate x just once
       (* ,x-val ,x-val ,x-val)
       )))

(defmacro ³ (x)
  "Macro to square expression X"
  (macroexp-let2 nil sym x
   `(* ,sym ,sym ,sym)))


(defun ÷ (num &rest divisors)
  "Floating point division."
  (let ((fdivisors (mapcar #'float divisors)))
    (apply #'/ num fdivisors)))


(defun ⅟ (divisor)
  "Shortcut for (/ (float DIVISOR)), returning reciprocal of DIVISOR."
  (/ (float divisor))
  )

(defun ½ (num)
  "Return half of NUM, converting to float when necessary."
  (/
   (or (int-even? num) (float num))
   2))


(defvar ½  0.5)

(defvar ⅓  0.3333333333333333)
(defvar ⅔  0.6666666666666667)

(defvar ¼  0.25)
(defvar ¾  0.75)

(defvar ⅕  0.2)
(defvar ⅖  0.4)
(defvar ⅗  0.6)
(defvar ⅘  0.8)

(defvar ⅙  0.16666666666666667)
(defvar ⅚  0.8333333333333333)

(defvar ⅐  0.14285714285714286)
(defvar ⅛  0.125)
(defvar ⅜  0.375)
(defvar ⅝  0.625)
(defvar ⅞  0.875)

(defvar ⅑  0.1111111111111111)

(defvar ⅒ 0.1)


(defun 𝟙 (val)
  "Truth function.
Returns 0 if VAL is nil, otherwise 1."
  (if val 1 0)
  )


;; ────────  ≈ For comparisons involving floating point numbers  ────────

;; Approximate equality of floating point numbers is a complicated subject.
;; I wrote the code below after consulting a randomascii article by Bruce Dawson
;; Comparing Floating Point Numbers, 2012 Edition

;; ≈small-abs-diff and ≈small-rel-diff may be set independently.
;; DBL_EPSILON in float.h might be a reasonable default for either.
(defvar ≈small-abs-diff 2.22045e-16
  "Smaller difference than this considered close enough."
  )
(defvar ≈small-rel-diff 2.22045e-16
  "Smaller relative difference than this considered close enough."
  )

(defun ≈2 (num1 num2)
  "Are floating point numbers NUM1 and NUM2 approximately numerically equal?
If so return NUM1, otherwise nil

If NUM1 and NUM2 are both integer types, `=' is used instead
to test for exact equality."
  (cl-check-type num1 number)
  (cl-check-type num2 number)
  (if (and (integerp num1)
           (integerp num2)
           (= num1 num2))
      num1
    ;; else compare as floating point numbers
    (let1 diff (abs (- num1 num2))
      (when
          (or
           (≦ diff ≈small-abs-diff)
           (≦ diff (* ≈small-rel-diff
                      (max (abs num1) (abs num2))
                      )))
        num1
        ))))


(defun ≈num-fun (base-num)
  "Return Predicate to test for number ≈ to BASE-NUM."
  #'(lambda (num)
      (≈2 base-num num)
      ))

(defun =num-fun (base-num)
  ;; For completeness, since we provide ≈num-fun
  "Return Predicate to test for number = to BASE-NUM."
  #'(lambda (num)
      (= base-num num)
      ))

(defun ≈ (num1 &rest more-nums)
  "If all nums are approximately equal to NUM1, return it;
otherwise nil"
  (when
      (seq-every-p
       (≈num-fun num1)
       more-nums
       )
    num1
    ))

(cl-defun ≈equal (obj1 obj2)
  "Like `equal', but numbers are compared with `≈2'.

Currently, always returns false for hash-tables,
this is consistent with `equal'."
  (cl-typecase obj1
    (cons
     (when (consp obj2)
       (and (≈equal (car obj1) (car obj2))
            (≈equal (cdr obj1) (cdr obj2))
            )
       ))
    (vector
     (and (vectorp obj2)
          (seqs-length=? obj1 obj2)
          (progn
            (dotimes (i (length obj1))
              (unless (≈equal (aref obj1 i) (aref obj2 i))
                (cl-return-from ≈equal
                  )))
            t)))
    (t
     (if (numberp obj1)
         (and (numberp obj2)
              (≈2 obj1 obj2))
       (equal obj1 obj2)
      ))))


(defun ≈int-or-same (num)
  "If number NUM is almost equal to an integer, return that integer;
otherwise return NUM as is."
  (or
   (≈ (round num) num)
   num
   ))


;; Chinese characters representing large numbers.
(defvar 万 10000)
(defvar 萬 10000)
(defvar 億 100000000)
(defvar 兆 1000000000000)


(provide 'shortcuts-math-libre)

;; Local Variables:
;; byte-compile-warnings: (not lexical)
;; End:

;;; shortcuts-math-libre.el ends here
