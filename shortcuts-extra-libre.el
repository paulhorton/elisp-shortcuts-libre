;;; shortcuts-extra-libre.el --- 

;; Copyright (C) 2019, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20190414
;; Updated: 20190414
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;; extra shortcuts not included in shortcuts-libre

;; Examples:
;;
;; Standard: (cl-every  (lambda (x) (>= 5))  NUM-LIST)
;;
;; Shortcut: (∀  (λ (≧ 5))  NUM-LIST)
;;
;;
;; Standard: (cl-some  (lambda (x) (not (= 5)))  NUM-LIST)
;;
;; Shortcut: (∃  (λ (x) (≠ 5))  NUM-LIST)
;;
;;; Change Log:

;;; Code:


(require 'cl-lib)


(defalias '∀ 'cl-every
  "Predicate true for All items?

Concise way to write `cl-every'.")


(defalias '∃ 'cl-some
  "Item Exists for which predicate is true?

Concise way to write `cl-every'.")



;;; shortcuts-extra-libre.el ends here
