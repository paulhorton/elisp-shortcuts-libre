;;; shortcuts-libre.el --- 

;; Copyright (C) 2019, Paul Horton.
;; License: This software may be used and/or distributed under the GNU General Public License
;; Contact:  ,(concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20190414
;; Updated: 20190414
;; Version: 0.0
;; Keywords: shorcut, alias, unicode,

;;; Commentary:

;; The shortcuts here are hopefully self-explanatory
;;
;; 'libre' because I freely use non-ascii characters.
;;
;;  Currently this file is empty.
;;
;;  Several definitions which used to live in this file have
;;  been moved to shortcuts-math-libre or shortcuts.
;;
;;  In the future it may again contain some definitions involving
;;  non-ascii characters in names which lack a mathematical context.
;;
;;;  See also:
;;     shortcuts
;;     shortcuts-match-libre
;;     shortucts-extra-libre
;;

;;; Change Log:

;;; Code:


(provide 'shortcuts-libre)


;;; shortcuts-libre.el ends here
