;;; shortcuts-math-libre-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231220
;; Updated: 20231220
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest ≈ ()
  (should
   (= 3
      (≈ 3 3.0000000000000004)
      )
   ))



;;; shortcuts-math-libre-tests.el ends here
